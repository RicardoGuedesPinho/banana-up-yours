﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class SpawnManager : MonoBehaviour {

	private GameObject powerUp;
	private float timeLeft = 5f;

	void Awake(){

		powerUp = Resources.Load ("Prefabs/powerUp") as GameObject;
	}

	void Update(){

		timeLeft -= Time.deltaTime;
		if (timeLeft < 0)
			spawnPowerUp();	
	}

	GameObject createPowerUp(float x, float y){

		GameObject instance = Instantiate(powerUp, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
		return instance;
	}

	public GameObject spawnPowerUp(){
		
		Vector2 spawnPos = new Vector2(Random.Range(-4f, 5f), Random.Range(-3f, 4f));
		GameObject instance = createPowerUp(spawnPos.x, spawnPos.y);
		timeLeft = Random.Range(5f, 10f);
		return instance;
	}
}
