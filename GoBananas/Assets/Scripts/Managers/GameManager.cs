﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	public enum GameState
	{
		MAIN_MENU_SCREEN,
		GAME_START,
		GAME_OVER
	}

	private List<string> twistStates = new List<string>(new string[] {"NO_TWIST","TWIST_INPUT","TWIST_GAMEPLAY"});
	private int currentTwistState;

	public static GameManager instance = null;

	//private Transform _game_area;

	void Awake(){
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);

		// To Create Children under this object
		//_game_area = new GameObject("GameArea").transform;
		currentTwistState = 2;

		// starting state machine
		changeState(GameState.MAIN_MENU_SCREEN);
	}

	public static void changeState (GameState newState){

		switch(newState){
			case GameState.MAIN_MENU_SCREEN:
				Debug.Log("Go to Main Menu Screen");
				break;
			case GameState.GAME_START:
				Debug.Log("Starting Game");
				//instance.GetComponent<SpawnManager>().spawnPlatforms(10, 90f);
				break;
			case GameState.GAME_OVER:
				Debug.Log("Game Over!");

				break;
			default:
			 	Debug.LogError("Unknown State");
			 	return;
		}
	}

	public static void killPlayer(Player player){
		Destroy(player.gameObject);
		changeState(GameState.GAME_OVER);
	}

	public int getCurrentTwistState(){
		return currentTwistState;
	}

	public List<string> getTwistStates(){
		return twistStates;
	}
}
