﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class HpPowerUp : MonoBehaviour {

	private int amount;
	Vector2 currentPos;
	Vector2 movement;
	Vector2 destination;

	void Awake(){
		amount = Random.Range(10, 21);
		currentPos = transform.position;
		movement = new Vector2(0,0);
		destination = new Vector2(0,0);

		destination = transform.position;
		movement = new Vector2(Random.Range(-2f, 3f), Random.Range(-1f, 2f));
		destination += movement;
	}

	void Update(){

		spawnAnimation();
	}

	void spawnAnimation(){

		/*float x = transform.position.x - destination.x;
		float y = transform.position.y - destination.y;
		float pit = Mathf.Sqrt((x*x)+(y*y));*/

		float dist = Vector2.Distance(transform.position,destination);
		if ( !(dist >= 0f && dist <= 0.2f)  ){
			movement = movement.normalized * 3 * Time.deltaTime;
			currentPos += movement;
			transform.position = currentPos;
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {

		Player collided = coll.gameObject.GetComponent<Player>() as Player;

		if (collided.tag == "Player"){
			// sendMessage to player with effect
			string currentTwistState = GameManager.instance.getTwistStates()[GameManager.instance.getCurrentTwistState()];
			if (currentTwistState.Equals("TWIST_GAMEPLAY")){
				collided.damagePlayer(amount);
			}else{
				collided.healPlayer(amount);
			}
			Destroy(this.gameObject);
		}

	}

}
