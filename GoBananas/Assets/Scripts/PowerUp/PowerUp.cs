﻿using UnityEngine;
using System.Collections;

public class PowerUp : MonoBehaviour {

	public GameObject hpPowerUp;
	public GameObject dmgPowerUp;
	// swap hp power up
	// more

	private int random;
	GameObject spawned;

	void spawnPowerUp(){

		random = Random.Range(0,1);

		switch (random){
			case 0:
				spawned = Instantiate(hpPowerUp, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity) as GameObject;
			break;
			case 1:
				spawned = Instantiate(dmgPowerUp, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity) as GameObject;
			break;
			default:
				Debug.Log("Unknown random powerup to spawn");
			return;
		}
	}

	void OnTriggerEnter2D(Collider2D coll) {

		Player collided = coll.gameObject.GetComponent<Player>() as Player;

		if (collided.tag == "Player"){
			// sendMessage to player with effect
			spawnPowerUp();
			Destroy(this.gameObject);

		}
	}

}
