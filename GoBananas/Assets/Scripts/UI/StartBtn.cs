﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class StartBtn : MonoBehaviour {

	Texture2D _over_mouse_btn; 
	bool _pressing_btn;

	void Awake(){

		//_over_mouse_btn = Resources.Load ("Resources/Sprites/main_menu/bt_start_highlight.png") as Texture2D;
		_pressing_btn = false;
		Debug.Log("");
	}

	void OnMouseOver(){
		// do change sprite effect	
	}

	void OnMouseDown(){
		_pressing_btn = true;
	}

	void OnMouseUp(){
		if(_pressing_btn){
			_pressing_btn = false;
			SceneManager.LoadScene("main");
		}
			
	}
}
