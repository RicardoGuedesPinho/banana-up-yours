﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHp : MonoBehaviour {

	public Sprite[] heartSprites;
	public Image currentHearts;

	public void updateHp(int hp){

		if(hp > 80 && hp <= 100)
			currentHearts.sprite = heartSprites[5];
		else if (hp > 60 && hp <= 80)
			currentHearts.sprite = heartSprites[4];
		else if (hp > 40 && hp <= 60)
			currentHearts.sprite = heartSprites[3];
		else if (hp > 20 && hp <= 40)
			currentHearts.sprite = heartSprites[2];
		else if (hp > 0 && hp <= 20)
			currentHearts.sprite = heartSprites[1];
		else if (hp <= 0)
			currentHearts.sprite = heartSprites[0];
		else 
			Debug.LogWarning("What happened with the hp?!");
	}
}
