﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
    private Vector2 moveDirection;

    /* Movement */
    public Rigidbody2D player;
    public string upKey;
    public string downKey;
    public string leftKey;
    public string rightKey;
    public float playerVelocity;

    /* Action */
    private int actionFlag = 0;
    public string atackKey;
    public string defenseKey;
    public GameObject projectile;
    public GameObject projectilePowa;
    public float projectileVelocity;
    public float projectilePowaVelocity;
    private float lastActionTime;
    private Vector3 shotPowaScale;
    GameObject shotPowa;

    // Use this for initialization
    void Start () {

        moveDirection = new Vector2(0,0);
        lastActionTime = Time.time;
    }
	
	// Update is called once per frame
	void Update () {

        movement();
        action();
    }
    void movement() {

        moveDirection.x = Input.GetAxis("Horizontal");
        moveDirection.y = Input.GetAxis("Vertical");
    }
    void action() {
        if (Input.GetKeyDown(KeyCode.Joystick1Button0))
        {
            if (actionFlag == 2)
            {
                actionFlag = 3;
            }
            else {
                actionFlag = 1;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Joystick1Button1))
        {
            actionFlag = 2;
            Debug.Log("2\n");
        }
        if(moveDirection != Vector2.zero)
        {
            if (Time.time - lastActionTime > 0.5)
            {
                if (actionFlag == 1)
                {
                    GameObject shot = Instantiate(projectile, this.transform.position, Quaternion.identity) as GameObject;
                    shot.GetComponent<Rigidbody2D>().velocity = moveDirection * projectileVelocity;
                    Destroy(shot, 5);
                    lastActionTime = Time.time;
                    actionFlag = 0;
                }
                else if (actionFlag == 3)
                {
                    shotPowa = Instantiate(projectilePowa, this.transform.position, Quaternion.identity) as GameObject;
                    shotPowa.GetComponent<Rigidbody2D>().velocity = moveDirection * projectilePowaVelocity;
                    shotPowaScale = shotPowa.transform.localScale;
                    Destroy(shotPowa, 5);
                    lastActionTime = Time.time;
                    actionFlag = 0;
                }
            }
        }
    }
    void FixedUpdate() {
        player.velocity = moveDirection * playerVelocity;
        if (shotPowa != null) {
            shotPowaScale.x += 0.7f;
            shotPowa.transform.localScale = shotPowaScale;
        }
    }
}
