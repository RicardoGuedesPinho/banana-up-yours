﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Player : MonoBehaviour {

	public float moveTime = 1f;
	public float speed = 6f;
	public float projectileSpeed = 10f;
	public int id = 1;

	private Animator anim;
	private BoxCollider2D boxCollider2D;
	private Rigidbody2D rb2D;
	//private float inverseMoveTime;
	private Vector2 moveDirection;
	private Vector2 currentPos;
	private GameObject projectile;
	private List<GameObject> projectiles;
	private Vector2 attackDirection;
	private Vector2 adjustProjectilePos;
	private Vector2 projectileCurrentPos;

	public int hp = 100;
	private PlayerHp hpUI;

	// Use this for initialization
	void Start () {

		currentPos = transform.position;
		anim = GetComponent<Animator>();
		//boxCollider2D = GetComponent<BoxCollider2D>();
		//rb2D = GetComponent<Rigidbody2D>();
		//inverseMoveTime = 1f/moveTime;
		moveDirection = new Vector2(0,0);
		attackDirection = new Vector2(0,0);
		projectileCurrentPos = new Vector2(0,0);
		adjustProjectilePos = new Vector2(2, -2);
		projectiles = new List<GameObject>();
		projectile = Resources.Load("Prefabs/Projectil") as GameObject;
		//hp = 100;
		hpUI = GetComponent<PlayerHp>() as PlayerHp;
	}
	
	// Update is called once per frame
	void Update () {

		movePlayer();
		attackPlayer();
		animatePlayer();
	}


	void FixedUpdate(){

		move();
		attack();
	}

	private void attackPlayer(){

		if((Input.GetKeyDown(KeyCode.Space) && id == 1) || (Input.GetKeyDown(KeyCode.KeypadEnter) && id == 2)){
			GameObject shot = Instantiate(projectile, transform.position, Quaternion.identity) as GameObject;
			projectiles.Add(shot);
			// Hammer
			if (id == 1){
				adjustProjectilePos.Set(0.5f, -0.5f);
				attackDirection.Set(1f, 0f);
			}
			else{
				adjustProjectilePos.Set(-0.5f, -0.5f);
				attackDirection.Set(-1f, 0f);
			} 

			projectileCurrentPos = shot.transform.position;
			projectileCurrentPos += adjustProjectilePos;
			shot.transform.position = projectileCurrentPos; 

			// will have to improve this as the player will have to forcelly stop attemting to walk during attack animation duration
			// also we cannot allow further shooting until animation completes
			moveDirection.Set(0f,0f);
			anim.SetTrigger("physicalAttack");
		}
	}

	private void attack(){

		attackDirection = attackDirection.normalized * projectileSpeed * Time.deltaTime;

		for(int i = 0; i < projectiles.Count; i++){
			GameObject shoot = projectiles[i];
			Vector2 currentProjectilePos = shoot.transform.position;
			currentProjectilePos += attackDirection;
			shoot.transform.position = currentProjectilePos;
		}
	}

	private void movePlayer(){

		float h, v;

		if ((Input.GetKey(KeyCode.A) && id == 1) || (Input.GetKey(KeyCode.LeftArrow) && id == 2)){
			h = -1;
		}else if((Input.GetKey(KeyCode.D) && id == 1) || (Input.GetKey(KeyCode.RightArrow) && id == 2)){
			h = 1;
		}else{
			h = 0;
		}

		if ((Input.GetKey(KeyCode.S) && id == 1) || (Input.GetKey(KeyCode.DownArrow) && id == 2)){
			v = -1;
		}else if((Input.GetKey(KeyCode.W) && id == 1) || (Input.GetKey(KeyCode.UpArrow) && id == 2)){
			v = 1;
		}else{
			v = 0;
		}


        	moveDirection.Set(h,v);

	}

	private void move(){

		// Normalise the movement vector and make it proportional to the speed per second.
		moveDirection = moveDirection.normalized * speed * Time.deltaTime;

		currentPos += moveDirection;
		transform.position = currentPos;
	}

	private void animatePlayer(){

		// Create a boolean that is true if either of the input axes is non-zero.
		bool walking = moveDirection.x != 0f || moveDirection.y != 0f;

		anim.SetBool("walk", walking);
	}

	public void damagePlayer(int amount){

		hp -= amount;
		updateHpUI();
		if (hp <= 0){
			killPlayer();
		}
	}

	public void healPlayer(int amount){

		if (hp + amount > 100){
			hp = 100;
		}else{
			hp += amount;
		}
		updateHpUI();
	}

	private void killPlayer(){

		GameManager.killPlayer(this);
	}

	private void updateHpUI(){

		hpUI.updateHp(hp);
	}
}
